package main

//go:generate go run github.com/swaggo/swag/cmd/swag init
//go:generate go run github.com/google/wire/cmd/wire

import (
	"bitbucket.org/nusatalent_dev/outsource-api/configs"
	"bitbucket.org/nusatalent_dev/outsource-api/shared/logger"
)

var config *configs.Config

func main() {
	// Initialize logger
	logger.InitLogger()

	// Initialize config
	config = configs.Get()

	// Set desired log level
	logger.SetLogLevel(config)

	// Wire everything up
	http := InitializeService()

	// consumers := InitializeEvent()

	// Start consumers
	// consumers.Start()

	// Run server
	http.SetupAndServe()
}
