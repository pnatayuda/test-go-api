//+build wireinject

package main

import (
	"bitbucket.org/nusatalent_dev/outsource-api/configs"
	"bitbucket.org/nusatalent_dev/outsource-api/infras"
	"bitbucket.org/nusatalent_dev/outsource-api/internal/domain/reminder"
	"bitbucket.org/nusatalent_dev/outsource-api/internal/domain/task"
	"bitbucket.org/nusatalent_dev/outsource-api/internal/domain/user"
	"bitbucket.org/nusatalent_dev/outsource-api/internal/handlers"
	"bitbucket.org/nusatalent_dev/outsource-api/transport/http"
	"bitbucket.org/nusatalent_dev/outsource-api/transport/http/middleware"
	"bitbucket.org/nusatalent_dev/outsource-api/transport/http/router"
	"github.com/google/wire"
)

// Wiring for configurations.
var configurations = wire.NewSet(
	configs.Get,
)

// Wiring for persistences.
var persistences = wire.NewSet(
	infras.ProvideMySQLConn,
)

var domainUser = wire.NewSet(
	user.ProvideUserServiceImpl,
	wire.Bind(new(user.UserService), new(*user.UserServiceImpl)),
	user.ProvideUserRepositoryMySQL,
	wire.Bind(new(user.UserRepository), new(*user.UserRepositoryMySQL)),
)

var domainTask = wire.NewSet(
	task.ProvideTaskServiceImpl,
	wire.Bind(new(task.TaskService), new(*task.TaskServiceImpl)),
	task.ProvideTaskRepositoryMySQL,
	wire.Bind(new(task.TaskRepository), new(*task.TaskRepositoryMySQL)),
)

var domainReminder = wire.NewSet(
	reminder.ProvideReminderServiceImpl,
	wire.Bind(new(reminder.ReminderService), new(*reminder.ReminderServiceImpl)),
	reminder.ProvideReminderRepositoryMySQL,
	wire.Bind(new(reminder.ReminderRepository), new(*reminder.ReminderRepositoryMySQL)),
)

// Wiring for all domains.
var domains = wire.NewSet(
	domainUser,
	domainTask,
	domainReminder,
)

var authMiddleware = wire.NewSet(
	middleware.ProvideAuthentication,
)

// Wiring for HTTP routing.
var routing = wire.NewSet(
	wire.Struct(new(router.DomainHandlers), "userHandler", "taskHandler", "reminderHandler"),
	handlers.ProvideUserHandler,
	handlers.ProviderTaskHandler,
	handlers.ProvideReminderHandler,
	router.ProvideRouter,
)

// Wiring for everything.
func InitializeService() *http.HTTP {
	wire.Build(
		// configurations
		configurations,
		// persistences
		persistences,
		// middleware
		authMiddleware,
		// domains
		domains,
		// routing
		routing,
		// selected transport layer
		http.ProvideHTTP)
	return &http.HTTP{}
}
