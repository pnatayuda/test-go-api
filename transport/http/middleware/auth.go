package middleware

import (
	"context"
	"net/http"

	"bitbucket.org/nusatalent_dev/outsource-api/infras"
	"bitbucket.org/nusatalent_dev/outsource-api/shared/auth"
	"bitbucket.org/nusatalent_dev/outsource-api/transport/http/response"
)

type Authentication struct {
	db *infras.MySQLConn
}

const (
	HeaderAuthorization = "Authorization"
)

func ProvideAuthentication(db *infras.MySQLConn) *Authentication {
	return &Authentication{
		db: db,
	}
}

func (a *Authentication) CheckToken(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get(HeaderAuthorization)
		claims, err := auth.ValidateToken(token)
		if err != nil {
			response.WithMessage(w, http.StatusUnauthorized, "token invalid")
			return
		}

		ctx := r.Context()
		ctx = context.WithValue(ctx, "user_id", claims.UserID)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
