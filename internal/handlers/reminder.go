package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/nusatalent_dev/outsource-api/internal/domain/reminder"
	"bitbucket.org/nusatalent_dev/outsource-api/shared"
	"bitbucket.org/nusatalent_dev/outsource-api/shared/failure"
	"bitbucket.org/nusatalent_dev/outsource-api/transport/http/middleware"
	"bitbucket.org/nusatalent_dev/outsource-api/transport/http/response"
	"github.com/go-chi/chi"
)

type ReminderHandler struct {
	ReminderService reminder.ReminderService
	AuthMiddleware  *middleware.Authentication
}

func ProvideReminderHandler(reminderService reminder.ReminderService, authMiddleware *middleware.Authentication) ReminderHandler {
	return ReminderHandler{
		ReminderService: reminderService,
		AuthMiddleware:  authMiddleware,
	}
}

func (h *ReminderHandler) Router(r chi.Router) {
	r.Route("/reminder", func(r chi.Router) {
		r.Group(func(r chi.Router) {
			r.Use(h.AuthMiddleware.CheckToken)
			r.Post("/new", h.Create)
			r.Post("/update", h.Update)
		})
	})
}

// Reminder create reminder.
// @Summary Create a new reminder.
// @Description This endpoint creates a new reminder.
// @Tags reminder
// @Param Authorization header string true "Bearer <token>"
// @Param parameter body reminder.ReminderRequestFormat true "reminder to be created."
// @Produce json
// @Success 201 {object} response.Base{data=reminder.ReminderResponseFormat}
// @Failure 400 {object} response.Base
// @Failure 409 {object} response.Base
// @Failure 500 {object} response.Base
// @Router /v1/reminder/new [post]
func (h *ReminderHandler) Create(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var requestFormat reminder.ReminderRequestFormat
	err := decoder.Decode(&requestFormat)
	if err != nil {
		response.WithError(w, failure.BadRequest(err))
		return
	}

	err = shared.GetValidator().Struct(requestFormat)
	if err != nil {
		response.WithError(w, failure.BadRequest(err))
		return
	}

	ctx := r.Context()

	idString := ctx.Value("user_id").(string)

	userID, _ := strconv.Atoi(idString)

	result, err := h.ReminderService.Create(requestFormat, userID)
	if err != nil {
		response.WithError(w, err)
		return
	}
	response.WithJSON(w, http.StatusCreated, result)
}

// Reminder update reminder.
// @Summary update a new reminder.
// @Description This endpoint update reminder.
// @Tags reminder
// @Param Authorization header string true "Bearer <token>"
// @Param parameter body reminder.ReminderRequestFormat true "reminder to be updated."
// @Produce json
// @Success 201 {object} response.Base{data=reminder.ReminderResponseFormat}
// @Failure 400 {object} response.Base
// @Failure 409 {object} response.Base
// @Failure 500 {object} response.Base
// @Router /v1/reminder/update [post]
func (h *ReminderHandler) Update(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var requestFormat reminder.ReminderRequestFormat
	err := decoder.Decode(&requestFormat)
	if err != nil {
		response.WithError(w, failure.BadRequest(err))
		return
	}

	err = shared.GetValidator().Struct(requestFormat)
	if err != nil {
		response.WithError(w, failure.BadRequest(err))
		return
	}

	ctx := r.Context()

	idString := ctx.Value("user_id").(string)

	userID, _ := strconv.Atoi(idString)

	result, err := h.ReminderService.Update(requestFormat, userID)
	if err != nil {
		response.WithError(w, err)
		return
	}
	response.WithJSON(w, http.StatusCreated, result)
}
