package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/nusatalent_dev/outsource-api/internal/domain/user"
	"bitbucket.org/nusatalent_dev/outsource-api/shared"
	"bitbucket.org/nusatalent_dev/outsource-api/shared/failure"
	"bitbucket.org/nusatalent_dev/outsource-api/transport/http/middleware"
	"bitbucket.org/nusatalent_dev/outsource-api/transport/http/response"
	"github.com/go-chi/chi"
)

// UserHandler is the HTTP handler for user domain.
type UserHandler struct {
	UserService    user.UserService
	AuthMiddleware *middleware.Authentication
}

// ProviceUserHandler is the provider for this handler.
func ProvideUserHandler(userService user.UserService, authMiddleware *middleware.Authentication) UserHandler {
	return UserHandler{
		UserService:    userService,
		AuthMiddleware: authMiddleware,
	}
}

// Router sets up the router for this domain.
func (h *UserHandler) Router(r chi.Router) {
	r.Route("/user", func(r chi.Router) {
		r.Post("/login", h.login)
		r.Post("/new", h.CreateUser)
		r.Group(func(r chi.Router) {
			r.Use(h.AuthMiddleware.CheckToken)
			r.Get("/detail/{id}", h.ResolveFooByID)
			r.Post("/change_password", h.ChangePassword)
		})
		// r.Put("/foo/{id}", h.UpdateFoo)
	})
}

// User Login.
// @Summary Login user.
// @Description This endpoint login user.
// @Tags user
// @Param user body user.UserLoginRequestFormat true "Login data."
// @Produce json
// @Success 201 {object} response.Base{data=user.UserResponseFormat}
// @Failure 400 {object} response.Base
// @Failure 409 {object} response.Base
// @Failure 500 {object} response.Base
// @Router /v1/user/login [post]
func (h *UserHandler) login(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var requestFormat user.UserLoginRequestFormat
	err := decoder.Decode(&requestFormat)
	if err != nil {
		response.WithError(w, failure.BadRequest(err))
		return
	}

	err = shared.GetValidator().Struct(requestFormat)
	if err != nil {
		response.WithError(w, failure.BadRequest(err))
		return
	}

	result, err := h.UserService.Login(requestFormat)
	if err != nil {
		response.WithError(w, err)
		return
	}

	response.WithJSON(w, http.StatusOK, result)
}

// User creates a new user.
// @Summary Create a new user.
// @Description This endpoint creates a new user.
// @Tags user
// @Param Authorization header string false "Bearer <token>"
// @Param user body user.UserRequestFormat true "The User to be created."
// @Produce json
// @Success 201 {object} response.Base{data=user.UserResponseFormat}
// @Failure 400 {object} response.Base
// @Failure 409 {object} response.Base
// @Failure 500 {object} response.Base
// @Router /v1/user/new [post]
func (h *UserHandler) CreateUser(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var requestFormat user.UserRequestFormat
	err := decoder.Decode(&requestFormat)
	if err != nil {
		response.WithError(w, failure.BadRequest(err))
		return
	}

	err = shared.GetValidator().Struct(requestFormat)
	if err != nil {
		response.WithError(w, failure.BadRequest(err))
		return
	}

	foo, err := h.UserService.Create(requestFormat)
	if err != nil {
		response.WithError(w, err)
		return
	}

	response.WithJSON(w, http.StatusCreated, foo)
}

// ResolveFooByID resolves a user by its ID.
// @Summary Resolve Product by ID
// @Description This endpoint resolves a user by its ID.
// @Tags user
// @Param Authorization header string true "Bearer <token>"
// @Param id path string true "The user's identifier."
// @Produce json
// @Success 200 {object} response.Base{data=user.UserResponseFormat}
// @Failure 400 {object} response.Base
// @Failure 404 {object} response.Base
// @Failure 500 {object} response.Base
// @Router /v1/user/detail/{id} [get]
func (h *UserHandler) ResolveFooByID(w http.ResponseWriter, r *http.Request) {
	idString := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idString)
	if err != nil {
		response.WithError(w, failure.BadRequest(err))
		return
	}

	foo, err := h.UserService.ResolveByID(id)
	if err != nil {
		response.WithError(w, err)
		return
	}

	response.WithJSON(w, http.StatusOK, foo)
}

// Change Password User.
// @Summary Change password user.
// @Description This endpoint to change password user.
// @Tags user
// @Param Authorization header string true "Bearer <token>"
// @Param user body user.UserChangePasswordFormat true "param data"
// @Produce json
// @Success 201 {object} response.Base
// @Failure 400 {object} response.Base
// @Failure 409 {object} response.Base
// @Failure 500 {object} response.Base
// @Router /v1/user/change_password [post]
func (h *UserHandler) ChangePassword(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var requestFormat user.UserChangePasswordFormat
	err := decoder.Decode(&requestFormat)
	if err != nil {
		response.WithError(w, failure.BadRequest(err))
		return
	}

	err = shared.GetValidator().Struct(requestFormat)
	if err != nil {
		response.WithError(w, failure.BadRequest(err))
		return
	}

	token, err := h.UserService.ChangePassword(requestFormat)
	if err != nil {
		response.WithError(w, err)
		return
	}

	response.WithJSON(w, http.StatusCreated, token)
}
