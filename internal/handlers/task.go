package handlers

import (
	"net/http"
	"strconv"

	"bitbucket.org/nusatalent_dev/outsource-api/internal/domain/task"
	"bitbucket.org/nusatalent_dev/outsource-api/shared/failure"
	"bitbucket.org/nusatalent_dev/outsource-api/transport/http/middleware"
	"bitbucket.org/nusatalent_dev/outsource-api/transport/http/response"
	"github.com/go-chi/chi"
)

type TaskHandler struct {
	TaskService    task.TaskService
	AuthMiddleware *middleware.Authentication
}

func ProviderTaskHandler(taskService task.TaskService, authMiddleware *middleware.Authentication) TaskHandler {
	return TaskHandler{
		TaskService:    taskService,
		AuthMiddleware: authMiddleware,
	}
}

func (h *TaskHandler) Router(r chi.Router) {
	r.Route("/task", func(r chi.Router) {
		r.Use(h.AuthMiddleware.CheckToken)
		r.Get("/list/{id}", h.GetByUser)
		r.Get("/detail/{id}", h.GetByID)
	})
}

//GetListByUser get list task by user ID.
// @Summary Get Task list by UserID
// @Description This endpoint for Get Task list by UserID.
// @Tags task
// @Param Authorization header string true "Bearer <token>"
// @Param id path string true "The user's ID"
// @Produce json
// @Success 200 {object} response.Base{data=[]task.TaskResponseFormat}
// @Failure 400 {object} response.Base
// @Failure 404 {object} response.Base
// @Failure 500 {object} response.Base
// @Router /v1/task/list/{id} [get]
func (h *TaskHandler) GetByUser(w http.ResponseWriter, r *http.Request) {
	idString := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idString)
	if err != nil {
		response.WithError(w, failure.BadRequest(err))
		return
	}

	foo, err := h.TaskService.GetByUser(id)
	if err != nil {
		response.WithError(w, err)
		return
	}

	response.WithJSON(w, http.StatusOK, foo)
}

//GetByID get task detail by ID.
// @Summary Get Task detail by ID
// @Description This endpoint for Get Task detail by ID.
// @Tags task
// @Param Authorization header string true "Bearer <token>"
// @Param id path string true "Task ID"
// @Produce json
// @Success 200 {object} response.Base{data=[]task.TaskResponseFormat}
// @Failure 400 {object} response.Base
// @Failure 404 {object} response.Base
// @Failure 500 {object} response.Base
// @Router /v1/task/detail/{id} [get]
func (h *TaskHandler) GetByID(w http.ResponseWriter, r *http.Request) {
	idString := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idString)
	if err != nil {
		response.WithError(w, failure.BadRequest(err))
		return
	}

	foo, err := h.TaskService.GetByID(id)
	if err != nil {
		response.WithError(w, err)
		return
	}

	response.WithJSON(w, http.StatusOK, foo)
}
