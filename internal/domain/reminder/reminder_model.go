package reminder

import (
	"time"

	"bitbucket.org/nusatalent_dev/outsource-api/shared"
	"github.com/guregu/null"
)

type Reminder struct {
	ID          int       `db:"id"`
	UserID      int       `db:"user_id" validate:"required"`
	ClockIn     null.Time `db:"clock_in"`
	ClockOut    null.Time `db:"clock_out"`
	ReminderDay string    `db:"reminder_day"`
	CreatedAt   time.Time `db:"created_at"`
	UpdatedAt   null.Time `db:"updated_at"`
}

type ReminderRequestFormat struct {
	ClockIn     string `json:"clock_in"`
	ClockOut    string `json:"clock_out"`
	ReminderDay string `json:"reminder_day"`
}

type ReminderResponseFormat struct {
	ID          int       `json:"id"`
	UserID      int       `json:"user_id"`
	ClockIn     null.Time `json:"clock_in"`
	ClockOut    null.Time `json:"clock_out"`
	ReminderDay string    `json:"reminder_day"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   null.Time `json:"updated_at"`
}

// Validate validates the entity.
func (u *Reminder) Validate() (err error) {
	validator := shared.GetValidator()
	return validator.Struct(u)
}

func (r Reminder) NewFromRequestFormat(req ReminderRequestFormat, userID int) (newReminder Reminder, err error) {
	timeLayout := "15:04"

	inTime, err := time.Parse(timeLayout, req.ClockIn)
	outTime, err := time.Parse(timeLayout, req.ClockOut)
	newReminder = Reminder{
		UserID:      userID,
		ClockIn:     null.TimeFrom(inTime),
		ClockOut:    null.TimeFrom(outTime),
		ReminderDay: req.ReminderDay,
		CreatedAt:   time.Now(),
	}

	err = newReminder.Validate()

	return
}

func (r Reminder) UpdateFromRequestFormat(req ReminderRequestFormat, userID int) (newReminder Reminder, err error) {
	timeLayout := "15:04"

	inTime, err := time.Parse(timeLayout, req.ClockIn)
	outTime, err := time.Parse(timeLayout, req.ClockOut)
	newReminder = Reminder{
		UserID:      userID,
		ClockIn:     null.TimeFrom(inTime),
		ClockOut:    null.TimeFrom(outTime),
		ReminderDay: req.ReminderDay,
		UpdatedAt:   null.TimeFrom(time.Now()),
	}

	err = newReminder.Validate()

	return
}

func (r Reminder) ToResponseFormat() ReminderResponseFormat {
	result := ReminderResponseFormat{
		ID:          r.ID,
		UserID:      r.UserID,
		ClockIn:     r.ClockIn,
		ClockOut:    r.ClockOut,
		ReminderDay: r.ReminderDay,
		CreatedAt:   r.CreatedAt,
	}

	return result
}
