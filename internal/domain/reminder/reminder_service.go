package reminder

import (
	"bitbucket.org/nusatalent_dev/outsource-api/configs"
	"bitbucket.org/nusatalent_dev/outsource-api/shared/failure"
)

type ReminderService interface {
	Create(request ReminderRequestFormat, userID int) (reminder Reminder, err error)
	Update(request ReminderRequestFormat, userID int) (reminder Reminder, err error)
}

type ReminderServiceImpl struct {
	ReminderRepository ReminderRepository
	Config             *configs.Config
}

func ProvideReminderServiceImpl(reminderRepository ReminderRepository, config *configs.Config) *ReminderServiceImpl {
	s := new(ReminderServiceImpl)
	s.ReminderRepository = reminderRepository
	s.Config = config
	return s
}

func (s *ReminderServiceImpl) Create(request ReminderRequestFormat, userID int) (reminder Reminder, err error) {
	reminder, err = reminder.NewFromRequestFormat(request, userID)

	if err != nil {
		return reminder, failure.BadRequest(err)
	}

	err = s.ReminderRepository.Create(reminder)
	if err != nil {
		return
	}

	return
}

func (s *ReminderServiceImpl) Update(request ReminderRequestFormat, userID int) (reminder Reminder, err error) {
	reminder, err = reminder.UpdateFromRequestFormat(request, userID)

	if err != nil {
		return reminder, failure.BadRequest(err)
	}

	err = s.ReminderRepository.Update(reminder)
	if err != nil {
		return
	}
	return
}
