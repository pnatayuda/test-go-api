package reminder

import (
	"strconv"

	"bitbucket.org/nusatalent_dev/outsource-api/infras"
	"bitbucket.org/nusatalent_dev/outsource-api/shared/failure"
	"bitbucket.org/nusatalent_dev/outsource-api/shared/logger"
	"github.com/jmoiron/sqlx"
)

var (
	reminderQueries = struct {
		selectReminder string
		insertReminder string
		updateReminder string
	}{
		selectReminder: `
			SELECT
				os_reminder.id,
				os_reminder.user_id,
				os_reminder.clock_in,
				os_reminder.clock_out,
				os_reminder.reminder_day,
				os_reminder.created_at,
				os_reminder.updated_at
			FROM os_reminder `,
		insertReminder: `
			INSERT INTO os_reminder (
				user_id,
				clock_in,
				clock_out,
				reminder_day,
				created_at
			) VALUES (
				:user_id,
				:clock_in,
				:clock_out,
				:reminder_day,
				:created_at)`,
		updateReminder: `
			UPDATE os_reminder
			SET
				os_reminder.clock_in = :clock_in,
				os_reminder.clock_out = :clock_out,
				os_reminder.reminder_day = :reminder_day,
				os_reminder.updated_at = :updated_at
			WHERE os_reminder.user_id = :user_id`,
	}
)

type ReminderRepository interface {
	Create(reminder Reminder) (err error)
	Update(reminder Reminder) (err error)
}

type ReminderRepositoryMySQL struct {
	DB *infras.MySQLConn
}

func ProvideReminderRepositoryMySQL(db *infras.MySQLConn) *ReminderRepositoryMySQL {
	s := new(ReminderRepositoryMySQL)
	s.DB = db
	return s
}

func (r *ReminderRepositoryMySQL) Create(reminder Reminder) (err error) {
	exists, err := r.ExistsByUserID(reminder.UserID)
	if err != nil {
		logger.ErrorWithStack(err)
		return
	}

	if exists {
		err = failure.Conflict("create", "reminder", "already exists")
		logger.ErrorWithStack(err)
		return
	}

	return r.DB.WithTransaction(func(tx *sqlx.Tx, e chan error) {
		if err := r.txCreate(tx, reminder); err != nil {
			e <- err
			return
		}
		e <- nil
	})
}

func (r *ReminderRepositoryMySQL) Update(reminder Reminder) (err error) {
	exists, err := r.ExistsByUserID(reminder.UserID)
	if err != nil {
		logger.ErrorWithStack(err)
		return
	}

	if !exists {
		err = failure.Conflict("update", "reminder", "not found")
		logger.ErrorWithStack(err)
		return
	}

	return r.DB.WithTransaction(func(db *sqlx.Tx, c chan error) {
		if err := r.txUpdate(db, reminder); err != nil {
			c <- err
			return
		}

		c <- nil
	})
}

func (r *ReminderRepositoryMySQL) ExistsByUserID(userID int) (exists bool, err error) {
	err = r.DB.Read.Get(
		&exists,
		"SELECT COUNT(user_id) FROM os_reminder WHERE user_id = ?",
		strconv.Itoa(userID))
	if err != nil {
		logger.ErrorWithStack(err)
	}
	return
}

//INTERNAL METHOD

// txCreate creates a Foo transactionally given the *sqlx.Tx param.
func (r *ReminderRepositoryMySQL) txCreate(tx *sqlx.Tx, reminder Reminder) (err error) {
	stmt, err := tx.PrepareNamed(reminderQueries.insertReminder)
	if err != nil {
		logger.ErrorWithStack(err)
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(reminder)
	if err != nil {
		logger.ErrorWithStack(err)
	}

	return
}

func (r *ReminderRepositoryMySQL) txUpdate(tx *sqlx.Tx, reminder Reminder) (err error) {
	stmt, err := tx.PrepareNamed(reminderQueries.updateReminder)
	if err != nil {
		logger.ErrorWithStack(err)
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(reminder)
	if err != nil {
		logger.ErrorWithStack(err)
	}
	return
}
