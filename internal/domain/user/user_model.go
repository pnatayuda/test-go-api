package user

import (
	"encoding/json"
	"time"

	"bitbucket.org/nusatalent_dev/outsource-api/shared"
	"bitbucket.org/nusatalent_dev/outsource-api/utility"
	"github.com/guregu/null"
)

//// Foo

// Foo is a sample parent entity model.
type User struct {
	ID          int         `db:"id"`
	Name        string      `db:"full_name" validate:"required"`
	Email       string      `db:"email" validate:"required"`
	Password    string      `db:"password" validate:"required"`
	Pic         null.String `db:"pic"`
	Address     null.String `db:"address"`
	PostalCode  null.String `db:"postal_code"`
	Gender      null.String `db:"gender"`
	PhoneNumber null.String `db:"phone_number"`
	BirthPlace  null.String `db:"birth_place"`
	Created     time.Time   `db:"created_at" validate:"required"`
	Updated     null.Time   `db:"updated_at"`
}

// IsDeleted checks whether a Foo is marked as deleted.
// func (u *User) IsDeleted() (deleted bool) {
// 	return u.Deleted.Valid
// }

// MarshalJSON overrides the standard JSON formatting.
func (u User) MarshalJSON() ([]byte, error) {
	return json.Marshal(u.ToResponseFormat())
}

// NewFromRequestFormat creates a new Foo from its request format.
func (u User) NewFromRequestFormat(req UserRequestFormat) (newUser User, err error) {
	hashedPassword, _ := utility.HashPassword(req.Password)
	newUser = User{
		Name:     req.Name,
		Email:    req.Email,
		Password: hashedPassword,
		Created:  time.Now(),
	}

	err = newUser.Validate()

	return
}

// SoftDelete marks a Foo as deleted by setting the "deleted" and "deletedBy"
// properties of a Foo.
// func (f *User) SoftDelete(userID uuid.UUID) (err error) {
// 	if f.IsDeleted() {
// 		return failure.Conflict("softDelete", "user", "already marked as deleted")
// 	}

// 	f.Deleted = null.TimeFrom(time.Now())

// 	return
// }

// ToResponseFormat converts this Foo to its response format.
func (u User) ToResponseFormat() UserResponseFormat {
	resp := UserResponseFormat{
		ID:          u.ID,
		Name:        u.Name,
		Email:       u.Email,
		Pic:         u.Pic,
		Address:     u.Address,
		PostalCode:  u.PostalCode,
		Gender:      u.Gender,
		PhoneNumber: u.PhoneNumber,
		BirthPlace:  u.BirthPlace,
		Created:     u.Created,
		Updated:     u.Updated,
	}
	return resp
}

// Validate validates the entity.
func (u *User) Validate() (err error) {
	validator := shared.GetValidator()
	return validator.Struct(u)
}

// UserRequestFormat represents a Foo's standard formatting for JSON deserializing.
type UserRequestFormat struct {
	Name     string `json:"full_name" validate:"required"`
	Email    string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type UserLoginRequestFormat struct {
	Email    string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type UserChangePasswordFormat struct {
	ID          int    `json:"id" db:"id" validate:"required"`
	OldPassword string `json:"old_password" db:"old_password" validate:"required"`
	NewPassword string `json:"new_password" db:"new_password" validate:"required"`
}

// FooResponseFormat represents a Foo's standard formatting for JSON serializing.
type UserResponseFormat struct {
	ID          int         `json:"id"`
	Name        string      `json:"full_name"`
	Email       string      `json:"email"`
	Pic         null.String `json:"pic"`
	Address     null.String `json:"address"`
	PostalCode  null.String `json:"postal_code"`
	Gender      null.String `json:"gender"`
	PhoneNumber null.String `json:"phone_number"`
	BirthPlace  null.String `json:"birth_place"`
	Created     time.Time   `json:"created_at"`
	Updated     null.Time   `json:"updated_at,omitempty"`
}
