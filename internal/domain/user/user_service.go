package user

import (
	"errors"
	"strconv"

	"bitbucket.org/nusatalent_dev/outsource-api/configs"
	"bitbucket.org/nusatalent_dev/outsource-api/shared/auth"
	"bitbucket.org/nusatalent_dev/outsource-api/shared/failure"
	"bitbucket.org/nusatalent_dev/outsource-api/utility"
)

// UserService is the service interface for User entities.
type UserService interface {
	Create(requestFormat UserRequestFormat) (user User, err error)
	ResolveByID(id int) (user User, err error)
	// SoftDelete(id int, userID int) (user User, err error)
	Login(requestFormat UserLoginRequestFormat) (token string, err error)
	ChangePassword(requestFormat UserChangePasswordFormat) (token string, err error)
	// Update(id int, requestFormat UserRequestFormat, userID int) (user User, err error)
}

// UserServiceImpl is the service implementation for User entities.
type UserServiceImpl struct {
	UserRepository UserRepository
	Config         *configs.Config
}

// ProvideUserServiceImpl is the provider for this service.
func ProvideUserServiceImpl(fooRepository UserRepository, config *configs.Config) *UserServiceImpl {
	s := new(UserServiceImpl)
	s.UserRepository = fooRepository
	s.Config = config

	return s
}

// Create creates a new User.
func (s *UserServiceImpl) Create(requestFormat UserRequestFormat) (user User, err error) {
	user, err = user.NewFromRequestFormat(requestFormat)
	if err != nil {
		return
	}

	if err != nil {
		return user, failure.BadRequest(err)
	}

	err = s.UserRepository.Create(user)

	if err != nil {
		return
	}

	return
}

// login user

func (s *UserServiceImpl) Login(requestFormat UserLoginRequestFormat) (token string, err error) {
	user, err := s.UserRepository.ResolveByEmail(requestFormat.Email)
	if err != nil {
		return "", errors.New("Email/password is invalid 1")
	}
	err = utility.CheckPassword(requestFormat.Password, user.Password)
	if err != nil {
		return "", errors.New("Email/password is invalid 2")
	}

	token, err = auth.GenerateToken(strconv.Itoa(user.ID))

	if err != nil {
		return "", errors.New("Email/password is invalid 3")
	}

	return
}

// ResolveByID resolves a User by its ID.
func (s *UserServiceImpl) ResolveByID(id int) (user User, err error) {
	user, err = s.UserRepository.ResolveByID(id)

	// if user.IsDeleted() {
	// 	return user, failure.NotFound("user")
	// }

	return
}

// ChangePassword
func (s *UserServiceImpl) ChangePassword(requestFormat UserChangePasswordFormat) (token string, err error) {

	user, err := s.UserRepository.ResolveByID(requestFormat.ID)
	if err != nil {
		return "", errors.New("user not found")
	}
	err = utility.CheckPassword(requestFormat.OldPassword, user.Password)
	if err != nil {
		return "", errors.New("old password is invalid")
	}

	hashedPassword, _ := utility.HashPassword(requestFormat.NewPassword)

	newUser := UserChangePasswordFormat{
		ID:          requestFormat.ID,
		OldPassword: requestFormat.OldPassword,
		NewPassword: hashedPassword,
	}

	err = s.UserRepository.ChangePassword(newUser)

	if err != nil {
		return
	}

	token, err = auth.GenerateToken(strconv.Itoa(requestFormat.ID))

	if err != nil {
		return "", errors.New("Error generate token")
	}

	return
}

// SoftDelete marks a User as deleted by setting its `deleted` and `deletedBy` properties.
// func (s *UserServiceImpl) SoftDelete(id int, userID int) (user User, err error) {
// 	user, err = s.UserRepository.ResolveByID(id)
// 	if err != nil {
// 		return
// 	}

// 	// err = user.SoftDelete(userID)
// 	// if err != nil {
// 	// 	return
// 	// }

// 	err = s.UserRepository.Update(user)
// 	return
// }

// // Update updates a User.
// func (s *UserServiceImpl) Update(id int, requestFormat UserRequestFormat, userID int) (user User, err error) {
// 	user, err = s.UserRepository.ResolveByID(id)
// 	if err != nil {
// 		return
// 	}

// 	err = user.Update(requestFormat, userID)
// 	if err != nil {
// 		return
// 	}

// 	err = s.UserRepository.Update(user)
// 	return
// }
