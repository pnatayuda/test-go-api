package user

import (
	"database/sql"
	"strconv"

	"bitbucket.org/nusatalent_dev/outsource-api/infras"
	"bitbucket.org/nusatalent_dev/outsource-api/shared/failure"
	"bitbucket.org/nusatalent_dev/outsource-api/shared/logger"
	"github.com/jmoiron/sqlx"
)

var (
	userQueries = struct {
		selectUser     string
		insertUser     string
		changePassword string
	}{
		selectUser: `
			SELECT
				os_user.id,
				os_user.full_name,
				os_user.email,
				os_user.password,
				os_user.pic,
				os_user.address,
				os_user.gender,
				os_user.postal_code,
				os_user.phone_number,
				os_user.birth_place,
				os_user.created_at,
				os_user.updated_at
			FROM os_user `,
		insertUser: `
			INSERT INTO os_user (
				full_name,
				email,
				password,
				created_at,
				updated_at
			) VALUES (
				:full_name,
				:email,
				:password,
				:created_at,
				:updated_at)`,
		changePassword: `
				UPDATE os_user
					SET
						password = :new_password
					WHERE id = :id
				`,
	}
)

// UserRepository is the repository for Foo data.
type UserRepository interface {
	Create(user User) (err error)
	ExistsByID(id int) (exists bool, err error)
	ResolveByID(id int) (user User, err error)
	ResolveByEmail(email string) (user User, err error)
	Update(user User) (err error)
	ChangePassword(user UserChangePasswordFormat) (err error)
}

// UserRepositoryMySQL is the MySQL-backed implementation of UserRepository.
type UserRepositoryMySQL struct {
	DB *infras.MySQLConn
}

// ProvideUserRepositoryMySQL is the provider for this repository.
func ProvideUserRepositoryMySQL(db *infras.MySQLConn) *UserRepositoryMySQL {
	s := new(UserRepositoryMySQL)
	s.DB = db
	return s
}

// Create creates a new Foo.
func (r *UserRepositoryMySQL) Create(user User) (err error) {
	exists, err := r.ExistsByID(user.ID)
	if err != nil {
		logger.ErrorWithStack(err)
		return
	}

	if exists {
		err = failure.Conflict("create", "user", "already exists")
		logger.ErrorWithStack(err)
		return
	}

	return r.DB.WithTransaction(func(tx *sqlx.Tx, e chan error) {
		if err := r.txCreate(tx, user); err != nil {
			e <- err
			return
		}

		e <- nil
	})
}

func (r *UserRepositoryMySQL) ChangePassword(user UserChangePasswordFormat) (err error) {
	exists, err := r.ExistsByID(user.ID)
	if err != nil {
		logger.ErrorWithStack(err)
		return
	}

	if !exists {
		err = failure.NotFound("user")
		logger.ErrorWithStack(err)
		return
	}

	return r.DB.WithTransaction(func(tx *sqlx.Tx, e chan error) {
		if err := r.txChangePassword(tx, user); err != nil {
			e <- err
			return
		}
		e <- nil
	})
}

// ExistsByID checks the existence of a Foo by its ID.
func (r *UserRepositoryMySQL) ExistsByID(id int) (exists bool, err error) {
	err = r.DB.Read.Get(
		&exists,
		"SELECT COUNT(id) FROM os_user WHERE os_user.id = ?",
		strconv.Itoa(id))
	if err != nil {
		logger.ErrorWithStack(err)
	}

	return
}

// ResolveByID resolves a Foo by its ID
func (r *UserRepositoryMySQL) ResolveByID(id int) (user User, err error) {
	err = r.DB.Read.Get(
		&user,
		userQueries.selectUser+" WHERE os_user.id = ?",
		strconv.Itoa(id))
	if err != nil && err == sql.ErrNoRows {
		err = failure.NotFound("user")
		logger.ErrorWithStack(err)
		return
	}
	return
}

//Resolve By Email
func (r *UserRepositoryMySQL) ResolveByEmail(email string) (user User, err error) {
	err = r.DB.Read.Get(
		&user,
		userQueries.selectUser+" WHERE os_user.email = ?",
		email)
	if err != nil && err == sql.ErrNoRows {
		err = failure.NotFound("user")
		logger.ErrorWithStack(err)
		return
	}
	return
}

// Update updates a Foo.
func (r *UserRepositoryMySQL) Update(user User) (err error) {
	exists, err := r.ExistsByID(user.ID)
	if err != nil {
		logger.ErrorWithStack(err)
		return
	}

	if !exists {
		err = failure.NotFound("user")
		logger.ErrorWithStack(err)
		return
	}
	return
}

// internal methods

// txCreate creates a Foo transactionally given the *sqlx.Tx param.
func (r *UserRepositoryMySQL) txCreate(tx *sqlx.Tx, user User) (err error) {
	stmt, err := tx.PrepareNamed(userQueries.insertUser)
	if err != nil {
		logger.ErrorWithStack(err)
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(user)
	if err != nil {
		logger.ErrorWithStack(err)
	}

	return
}

//txUpdate change password user

func (r *UserRepositoryMySQL) txChangePassword(tx *sqlx.Tx, user UserChangePasswordFormat) (err error) {
	stmt, err := tx.PrepareNamed(userQueries.changePassword)
	if err != nil {
		logger.ErrorWithStack(err)
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(user)
	if err != nil {
		logger.ErrorWithStack(err)
	}
	return
}
