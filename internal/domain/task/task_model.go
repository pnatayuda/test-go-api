package task

import (
	"time"

	"github.com/guregu/null"
)

type Task struct {
	ID        int       `db:"id"`
	UserID    int       `db:"user_id"`
	Type      string    `db:"type"`
	StartDate time.Time `db:"start_date"`
	EndDate   time.Time `db:"end_date"`
	Map       string    `db:"map"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt null.Time `db:"updated_at"`
}

type TaskRequestFormat struct {
	ID        int       `json:"id"`
	UserID    int       `db:"user_id"`
	Type      string    `json:"type"`
	StartDate null.Time `json:"start_date"`
	EndDate   null.Time `json:"end_date"`
	Map       string    `json:"map"`
}

type TaskResponseFormat struct {
	ID        int       `json:"id"`
	UserID    int       `json:"user_id"`
	Type      string    `json:"type"`
	StartDate null.Time `json:"start_date"`
	EndDate   null.Time `json:"end_date"`
	Map       string    `json:"map"`
}
