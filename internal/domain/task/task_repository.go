package task

import (
	"database/sql"

	"bitbucket.org/nusatalent_dev/outsource-api/infras"
	"bitbucket.org/nusatalent_dev/outsource-api/shared/failure"
	"bitbucket.org/nusatalent_dev/outsource-api/shared/logger"
)

var (
	taskQueries = struct {
		selectTask string
	}{
		selectTask: `
			SELECT
				os_task.id,
				os_task.user_id,
				os_task.type,
				os_task.start_date,
				os_task.end_date,
				os_task.map,
				os_task.created_at
			FROM os_task `,
	}
)

type TaskRepository interface {
	GetByUser(userID int) (tasks []Task, err error)
	GetByID(taskID int) (task Task, err error)
}

type TaskRepositoryMySQL struct {
	DB *infras.MySQLConn
}

func ProvideTaskRepositoryMySQL(db *infras.MySQLConn) *TaskRepositoryMySQL {
	s := new(TaskRepositoryMySQL)
	s.DB = db
	return s
}

func (r *TaskRepositoryMySQL) GetByUser(userID int) (tasks []Task, err error) {
	err = r.DB.Read.Select(
		&tasks,
		taskQueries.selectTask+"WHERE os_task.user_id = ?",
		userID)
	if err != nil && err == sql.ErrNoRows {
		err = failure.NotFound("task")
		logger.ErrorWithStack(err)
	}
	return
}

func (r *TaskRepositoryMySQL) GetByID(taskID int) (task Task, err error) {
	err = r.DB.Read.Get(&task, taskQueries.selectTask+"WHERE id = ?", taskID)
	if err != nil {
		logger.ErrorWithStack(err)
	}
	return
}
