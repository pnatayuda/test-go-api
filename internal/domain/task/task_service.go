package task

import (
	"bitbucket.org/nusatalent_dev/outsource-api/configs"
)

type TaskService interface {
	GetByUser(userID int) (tasks []Task, err error)
	GetByID(taskID int) (task Task, err error)
}

type TaskServiceImpl struct {
	TaskRepository TaskRepository
	Config         *configs.Config
}

func ProvideTaskServiceImpl(taskRepository TaskRepository, config *configs.Config) *TaskServiceImpl {
	s := new(TaskServiceImpl)
	s.TaskRepository = taskRepository
	s.Config = config

	return s
}

func (s *TaskServiceImpl) GetByUser(userID int) (tasks []Task, err error) {
	tasks, err = s.TaskRepository.GetByUser(userID)
	if err != nil {
		return
	}
	return
}

func (s *TaskServiceImpl) GetByID(taskID int) (task Task, err error) {
	task, err = s.TaskRepository.GetByID(taskID)
	if err != nil {
		return
	}
	return

}
