package auth

import (
	"errors"
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
)

const (
	secretKey = "vINSHsdG"
)

type JwtClaim struct {
	UserID string
	jwt.StandardClaims
}

func GenerateToken(userID string) (token string, err error) {
	fmt.Println(userID)
	claims := &JwtClaim{
		UserID: userID,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * time.Duration(24)).Unix(),
			Issuer: "authservice",
		},
	}

	newToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	token, err = newToken.SignedString([]byte(secretKey))
	if err != nil {
		return
	}

	return
}

func ValidateToken(signedToken string) (claims *JwtClaim, err error) {
	token, err := jwt.ParseWithClaims(
			signedToken,
			&JwtClaim{},
			func(token *jwt.Token) (interface{}, error) {
			return []byte(secretKey), nil
		},
	)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	claims, ok := token.Claims.(*JwtClaim)
	if !ok {
		err = errors.New("token is invalid")
	}

	if claims.ExpiresAt < time.Now().Unix() {
		err = errors.New("JWT is expired")
		return
	}
return
}